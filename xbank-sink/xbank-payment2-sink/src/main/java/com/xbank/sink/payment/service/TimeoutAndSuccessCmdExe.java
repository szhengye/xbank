package com.xbank.sink.payment.service;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizResultEnum;
import com.bizmda.bizsip.sink.cmdexe.AbstractSinkBeanCmdExe;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class TimeoutAndSuccessCmdExe extends AbstractSinkBeanCmdExe {
    private int count = 0;
    
    @Override
    public JSONObject execute(JSONObject jsonObject) throws BizException {
        String tranCode = (String)jsonObject.get("tranCode");
        this.count++;
        if (this.count > 3) {
            this.count = 0;
            log.info("交易:{},返回交易成功!",tranCode);
            return jsonObject;
        } else {
            log.info("交易:{},返回超时异常!",tranCode);
            throw new BizException(BizResultEnum.RETRY_DELAY_APP_SERVICE);
        }
    }
}
