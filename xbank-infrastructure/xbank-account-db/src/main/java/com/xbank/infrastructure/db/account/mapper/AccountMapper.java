package com.xbank.infrastructure.db.account.mapper;

import com.xbank.infrastructure.db.account.domain.Account;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.xbank.infrastructure.db.account.domain.Account
 */
public interface AccountMapper extends BaseMapper<Account> {

}




