package com.xbank.infrastructure.db.customer.service;

import com.xbank.infrastructure.db.customer.domain.Customer;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface CustomerService extends IService<Customer> {

}
