package com.xbank.source.xml;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

/**
 * @author 史正烨
 */
@Slf4j
@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
@ComponentScan(basePackages={"cn.hutool.extra.spring"
        ,"com.xbank.source.xml.controller"})
@Import(cn.hutool.extra.spring.SpringUtil.class)
public class XmlSourceApplication {
    public static void main(String[] args) {
        SpringApplication.run(XmlSourceApplication.class, args);
    }
}
