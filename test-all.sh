#!/bin/sh
set -v on
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/sink/customer" -X POST --data '{"methodName":"getCustomer","params":["001"]}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/sink/account" -X POST --data '{"methodName":"getAccountListByCustomerId","params":["001"]}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/app/personal" -X POST --data '{"methodName":"getCustomerAndAccountList","params":["001"]}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/sink/customer" -X POST --data '{"methodName":"getCustomer","params":["001"]}' http://localhost:8888/api|jq
curl http://localhost:9001/personal/getCustomer\?customerId=001|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/sink/account" -X POST --data '{"methodName":"getAccountListByCustomerId","params":["001"]}' http://localhost:8888/api|jq
curl http://localhost:9001/personal/getAccountListByCustomerId\?customerId=001|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/app/personal" -X POST --data '{"methodName":"getCustomerAndAccountList","params":["001"]}' http://localhost:8888/api|jq
curl http://localhost:9001/personal/getCustomerAndAccountList\?customerId=001|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/sink/payment1" -X POST --data '{"message":"Hello world!"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/app/personal" -X POST --data '{"methodName":"send2Payment1","params":["Hello world!"]}' http://localhost:8888/api|jq
curl http://localhost:9001/personal/send2Payment1\?message=hello|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/app/personal" -X POST --data '{"methodName":"getCustomerAndSaf2Payment2","params":["timeout","001"]}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/app/personal" -X POST --data '{"methodName":"getCustomerAndSaf2Payment2","params":["3timeout-fail","001"]}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/app/personal" -X POST --data '{"methodName":"getCustomerAndSaf2Payment2","params":["3timeout-success","001"]}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/app/personal" -X POST --data '{"methodName":"payoutForward","params":["3timeout-success","0001",1]}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/app/personal" -X POST --data '{"methodName":"payoutForward","params":["3timeout-fail","0001",1]}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/app/personal" -X POST --data '{"methodName":"payoutBackward","params":["3timeout-success","0001",1]}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/app/personal" -X POST --data '{"methodName":"payoutBackward","params":["3timeout-fail","0001",1]}' http://localhost:8888/api|jq
curl -H "Content-Type:application/xml" -X POST --data '<customerId>001</customerId>' http://localhost:9002/personal/getCustomerAndAccountList
curl -H "Content-Type:application/xml" -X POST --data '<customerId>001</customerId>' http://localhost:9002/personal/getAccountListByCustomerId


